package rockPaperScissors;
import org.w3c.dom.ls.LSOutput;

import java.util.Random;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    Random generator = new Random();
    int computerInt;
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    String result;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public boolean isWinner(String player_1, String player_2){
        if (player_1.equals("rock")){
            return player_2.equals("scissors");
        }
        else if (player_1.equals("scissors")){
            return player_2.equals("paper");
        }
        else return player_2.equals("rock");
    }


    
    public void run() {

        // TODO: Implement Rock Paper Scissors
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            roundCounter++;
            String playerChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            String aiChoice = rpsChoices.get(generator.nextInt(3));

            if (playerChoice.equals("quit")) {
                break;
            }

            if (!rpsChoices.contains(playerChoice)) {
                System.out.println("I don't understand " + playerChoice + ". Could you try again?");
                continue;
            }

            if (isWinner(playerChoice, aiChoice)) {
                result = "Human win!";
                humanScore++;
            } else if (isWinner(aiChoice, playerChoice)) {
                result = "Computer win!";
                computerScore++;
            } else if (isWinner(playerChoice, aiChoice) == isWinner(aiChoice, playerChoice)) {
                result = "It's a tie!";
            }

            System.out.println("Human chose " + playerChoice + ", " + "computer chose " + aiChoice + ". " + result);
            System.out.println("Score: " + "human " + humanScore + ", " + "computer " + computerScore);
            String keepPlaying = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (keepPlaying.equals("y")) {
                continue;
            }
            if (keepPlaying.equals("n")){
                break;
            }


        }
        System.out.println("Bye bye :)");

        }


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
